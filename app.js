const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');
const ul = document.createElement('ul');


books.forEach(book => {
    try {
        if (!book.author || !book.name || !book.price) {
            const missingProperties = [];

            if (!book.author) {
                missingProperties.push("author");
            }
            if (!book.name) {
                missingProperties.push("name");
            }
            if (!book.price) {
                missingProperties.push("price");
            }

            throw new Error(`Missing properties in book object (${missingProperties.join(', ')}): ${JSON.stringify(book)}`);
        }

        const li = document.createElement('li');
        li.textContent = `${book.author}, ${book.name}, ${book.price}`;
        ul.appendChild(li);

    } catch (error) {
        console.error(error.message);
    }
});



root.appendChild(ul);